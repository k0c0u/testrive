// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestRiveGameMode.generated.h"

UCLASS(minimalapi)
class ATestRiveGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATestRiveGameMode();
};



