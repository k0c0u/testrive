// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestRiveGameMode.h"
#include "TestRiveCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATestRiveGameMode::ATestRiveGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
